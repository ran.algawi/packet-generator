# Packet generator

This packet generator is used for testing and teaching how to construct proper packets and create client-server sessions.

## Changes by version

### Version 1.0
- Add simple handling for sending and receiving packets.

### Version 0.85
- Remove un-used variable.

### Version 0.8
- Program will capture packets with the ethernet header.

### Version 0.7
- Add functions for capturing and parsing packets.
- Add feature listening to UDP traffic.
- Add feature listening to TCP traffic.
- Beautify help menu prints.

### Version 0.5
- Add function to construct IP header.
- Add function to construct UDP packets.
- Remove unused variables.
- Make packet id number configurable.

### Version 0.1
- Add function to construct TCP packets.
- Add function to send packets.
- Add function to dump packets.
