#include "generator.h"
/**
 * @brief This function is used to print a simple welcome for user.
 *
 */
void print_welcome(){
	printf("\nWelcome to the Packet Generator V1.2\n");
	printf("Developer: Ran Algawi, ran@notyourtypicalprogrammer.net\n\n");
}

/**
 * @brief This function is used to print the flags of the generator.
 *
 */
void print_help(){
	print_welcome();
	printf("Options:\n");
	printf("-r        -> Amount of rounds to perform, Input: Whole numbers i.e. int\n");
	printf("-i        -> Delay between rounds in seconds, Input: All real numbers, e.g. 1, 0.5, 0.7, 0.001\n");
	printf("-f        -> Flood, If used, generator will override -r,-i and -pc values, No input.\n");
	printf("-t        -> Traffic kind, Input: tcp\n");
	printf("-v        -> Verbose, No input.\n");
	printf("-s        -> Show logs of sending packets, no input.\n");
	printf("-pc       -> Amount of packets to send per round, Input: Whole numbers i.e. int\n");
	printf("-ip       -> Destination IPv4 address, Input: 127.0.0.1, 10.135.83.1\n");
	printf("-sp       -> Source port, Input: Whole number i.e. int\n");
	printf("-dp       -> Destination port, Input: Whole number i.e. int\n");
	printf("-nic      -> Source interface, Input: name of source interface, e.g. eth0, enp0s0, eno1\n");
	printf("-mac      -> Desitnation MAC address, Input: 1f:ab:c2:da:ff:fc, 1F:AB:C2:DA:FF:FC\n");
	printf("--time    -> Amount of time to transmit traffic for. \n             This option will override rounds(-r), Input: time in seconds, e.g. 30, 60.\n");
	printf("--safe    -> Safety on. If used and some paramters are missing, generator will use default values.\n");
	printf("--ecodes  -> Print error codes\n");
	printf("--listen  -> Listen to traffic\n");
	printf("--payload -> Data to attach to the packet, Input: Anything inside quotes. (e.g. \"Hello there\")\n\n");
	printf("Default values:\n");
	printf("Rounds: 10, Packets: 100, Round interval: 3 seconds, Traffic time: 60 seconds.\n\n");

}

/**
 * @brief This function is used to print the values the generator is using.
 *
 * @param pc Packet count.
 * @param r Number of rounds.
 * @param i Time between intervals.
 * @param nic Name of source network interface.
 * @param ip destination IP address.
 * @param mac destination MAC address.
 * @param traffic_kind What traffic the user wants to generate.
 * @param traffic_time For how long the user wants the traffic generator to run.
 */
void print_variables_used_for_generator(int pc, int r, float i, char* nic, char* ip, char* mac, char* traffic_kind, int
traffic_time){
	print_welcome();
	printf("Using the following variables for generator:\n============================================\n");
	if (traffic_time == -1){
		printf("Generating %d %s packets every round for %d rounds.\n", pc, traffic_kind, r);
		printf("Delay between rounds: %.2f seconds.\n", i);
	}
	else
		printf("Generating %d %s packets every %.2f seconds for a total of %d seconds\n", pc, traffic_kind, i, traffic_time);

	if(mac != NULL)
		printf("Target:\n  Mac addr: %s\n  IP addr: %s\n\n", mac, ip);
	else
		printf("Target:\n  IP addr: \%s\n\n", ip);
}

/**
 * @brief This function is used to convert hex char into it's ASCII value
 *
 * @param ch The hex character the user wants to convert.
 * @return unsigned char ASCII value of the hex character.
 */
unsigned char hex_digit( char ch )
{
    if(             ( '0' <= ch ) && ( ch <= '9' ) ) { ch -= '0'; }
    else
    {
        if(         ( 'a' <= ch ) && ( ch <= 'f' ) ) { ch += 10 - 'a'; }
        else
        {
            if(     ( 'A' <= ch ) && ( ch <= 'F' ) ) { ch += 10 - 'A'; }
            else                                     { ch = 16; }
        }
    }
    return ch;
}

/**
 * @brief This function is used to calculate checksum.
 *
 * @param ptr Pointer to the data the user wants to perform checksum on.
 * @param nbytes The length of the checksum the user require.
 * @return unsigned short Checksum result.
 */
unsigned short csum(unsigned short *ptr,int nbytes) {
	register long sum;
	unsigned short oddbyte;

	sum=0;
	while(nbytes>1) {
		sum+=*ptr++;
		nbytes-=2;
	}
	if(nbytes==1) {
		oddbyte=0;
		*((u_char*)&oddbyte)=*(u_char*)ptr;
		sum+=oddbyte;
	}

	sum = (sum>>16)+(sum & 0xffff);
	sum = sum + (sum>>16);

	return (short) ~sum;
}

/**
 * @brief This function is used to get the IP address
 * assigned to a network interface.
 *
 * @param interface_name The name of the network interface.
 * @return char* IP of network interface.
 */
char* get_ip_of_interface(char* interface_name){
	int fd;
	struct ifreq ifr;

	fd = socket(AF_INET, SOCK_DGRAM, 0);
	ifr.ifr_addr.sa_family = AF_INET;
	strncpy(ifr.ifr_name, interface_name, IFNAMSIZ-1);
	ioctl(fd, SIOCGIFADDR, &ifr);
	close(fd);
	return inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr);
}

/**
 * @brief This function is used to convert a string into lower alphabets
 *
 * @param str The string the user wants to convert.
 */
void lower_string(char* str){

	for(int idx=0; str[idx]; idx++)
		str[idx] = tolower(str[idx]);

}

/**
 * @brief This function is used to print the exit codes this program may return
 *
 */
void print_exit_codes(){
	print_welcome();
	printf("Exit code 0  -> No errors were encountered\n");
	printf("Exit code 1  -> Error was encoutered and program was terminated\n");
	printf("Exit code 40 -> Missing argument in command\n");
	printf("Exit code 5  -> Failed to send packet\n");
	printf("Exit code 6  -> Packet meant to be sent has negative length\n");
	printf("Exit code 7  -> Packet meant to be sent has zero length\n");
}

/**
 * @brief This function is used to open a socket for transmiting.
 * The socket will be opened and assigned the IP_HDRINCL flag.
 * Also, the function will set a handler for termination function
 * so the socket will be always be closed.
*/
void open_tx_socket(){
	int one=1;
	const int *val = &one;
	struct sigaction sig_handler;

	
	if ((tx_socketfd = socket(PF_INET, SOCK_RAW, IPPROTO_RAW)) < 0){
		perror("socket() error");
		exit(-1);
	}
	else if (verbose_flag)
		printf("Socket was opened successfully\n");

	// IP_HDRINCL will inform the socket that the IP header is included
	// in the packet we're going to send.
	if (setsockopt(tx_socketfd, IPPROTO_IP, IP_HDRINCL, val, sizeof(one)) < 0){
		perror("Failed to turn IP_HDRINCL flag on");
		exit(-1);
	}
	else if (verbose_flag)
		printf("IP_HDRINCL flag was turned on for socket\n");

	// Bind socket to a specific interface.
	if(setsockopt(tx_socketfd, SOL_SOCKET, SO_BINDTODEVICE, (void*) &ifr, sizeof(ifr)) < 0){
		char err_str[256] = "Failed to bind socket to interface ";
		strcat(err_str, "\n");
		perror(err_str);
		exit(EXIT_FAILURE);
	}
	else if(verbose_flag)
		printf("Socket was binded successfuly\n");

	// Set signal for ctrl+c or program self termination to close 
	// the socket we opened.
	sig_handler.sa_handler = close_down;
	sigemptyset(&sig_handler.sa_mask);
	sig_handler.sa_flags = 0;
	sigaction(SIGINT, &sig_handler, NULL);

}

/**
 * @brief This is the handler for termination signals.
 * It is very important to close a socket when you finish with it.
 *
 * @param sigtype Type of signal.
 */
void close_down(int sigtype)
{
  close(tx_socketfd);
  if (verbose_flag)
	  printf("Socket closed.");
  exit(EXIT_SUCCESS);
}

/**
 * @brief This function is used to construct a TCP packet.
 *
 * @param dst_ip Destination IP.
 * @param dst_port Destination port.
 * @param src_ip Source IP.
 * @param src_port Source port.
 * @param payload Data to attach at end of the  packet (e.g. message)
 * @param sequence_num Sequence number of the packet.
 * @param syn_flag 1 or 0, Turn on or off the sync flag.
 * @param ack_flag 1 0r 0, Turn on or off the acknowledgement flag.
 * @param ack_num Acknowledge number.
 * @param fin_flag 1 or 0, Tell reciever to terminate connection.
 * @param rst_flag 1 or 0, Tell reciever to reset connection.
 * @param pkt_id Id number of the packet.
 * @return char* The constructed TCP packet without ethernet frame.
 */
char *construct_tcp_packet(char dst_ip[32], int dst_port, char src_ip[32],
int src_port, char *payload, int sequence_num, int syn_flag, int ack_flag,
int ack_num, int fin_flag, int rst_flag, int pkt_id){

	// Define variables
	// datagram will be used to represent packet.
	// data will be used to represent data we want to send.
	// pseudogram will represent the TCP checksum.
	char *datagram, *pseudogram, *data;
	// Our IP header.
	struct iphdr *iph;
	// Our TCP header.
	struct tcphdr *tcph;
	// Pseudo header will be used to calculate the TCP checksum.
	struct pseudo_header psh;
	int psize;

	// Allocate memory for packet
	datagram = (char*) calloc(MAX_PKTLEN, 1);

	// Add data
	data = datagram + sizeof(struct iphdr) + sizeof(struct tcphdr);
	strcpy(data, payload);

	// Fill in IP header
	iph = construct_ipv4_header(datagram, src_ip, dst_ip, IPPROTO_TCP, pkt_id);
	iph->tot_len = sizeof(struct iphdr) + sizeof(struct tcphdr) + strlen(data);

	// TCP header
	tcph = (struct tcphdr*) (datagram + sizeof(struct iphdr));
	tcph->source = htons(src_port); 	  // Source port
	tcph->dest = htons(dst_port);	 	 // Destination port
	tcph->seq = htonl(sequence_num);	// Sequence number, need to change
	tcph->ack_seq = ack_num;
	tcph->doff = TCP_OFFSET;		  // Offset, size of header without data
	tcph->fin = fin_flag;
	tcph->syn=syn_flag;
	tcph->rst=rst_flag;
	tcph->ack=ack_flag;
	tcph->window = htons(5840); // Maximum allowed window size

	// IP Checksum
	iph->check = csum((unsigned short*) datagram, iph->tot_len);

	// TCP checksum
	psh.source_address = inet_addr(src_ip);
	psh.dest_address = inet_addr(dst_ip);
	psh.placeholder = 0;
	psh.protocol = IPPROTO_TCP;
	psh.pkt_length = htons(sizeof(struct tcphdr) + strlen(data));

	psize = sizeof(struct pseudo_header) + sizeof(struct tcphdr) + strlen(data);
	pseudogram = malloc(psize);

	memcpy(pseudogram , (char*) &psh , sizeof (struct pseudo_header));
	memcpy(pseudogram + sizeof(struct pseudo_header) , tcph , sizeof(struct tcphdr) + strlen(data));

	tcph->check = csum( (unsigned short*) pseudogram , psize);
	pkt_len = iph->tot_len;
	return datagram;
}

/**
 * @brief Setup the target IPv4 interface object and source interface object.
 *
 * @param port Destination port.
 * @param ip Destination IP.
 * @param interface_name Name of source interface.
 */
void setup_v4_interface(int port, char *ip, char* interface_name){
	inter.sin_family = AF_INET;
	inter.sin_port = htons(port);
	inter.sin_addr.s_addr = inet_addr(ip);

	memset(&ifr, 0, sizeof(ifr));
	strcpy(ifr.ifr_name, interface_name);
}

/**
 * @brief This function is used to send packets.
 *
 * @param datagram Content of packet we want to send.
 * @param iph_len Length of the packet without ethernet frame.
 */
void send_packet(char *datagram, int iph_len){

	int ret = 0,i;
	struct sockaddr *d_sin = (struct sockaddr*) &inter;

	ret = sendto(tx_socketfd, datagram, iph_len, 0, d_sin, sizeof(inter));
	if(ret < 0){
		perror("Failed to send packet");
		if (verbose_flag)
			packet_dump("TX packet dump", datagram, iph_len);
		exit(PKT_SEND_FAIL);
	}
	if(verbose_flag)
		packet_dump("TX packet dump", datagram, iph_len);
}

/**
 * @brief This function is used to print the hex dump of a packet.
 *
 * @param desc Description of dump. (e.g. TX/RX packet.)
 * @param pkt Content of packets which was sent/recieved.
 * @param pkt_len Length of packet without ethernet frame.
 */
void packet_dump (const char *desc, const void *pkt, const int pkt_len){
    int i;
    unsigned char buff[17];
    const unsigned char * pc = (const unsigned char *)pkt;

    printf ("%s:\n", desc);

    // Length checks.
    if(pkt_len == 0){
        perror("Packet has 0 length");
        exit(PKT_ZERO_LEN);
    }
    else if(pkt_len < 0){
		perror("Packet has negative length");
		exit(PKT_NEG_LEN);
    }

    // Go over the packet's data
    for(i = 0; i < pkt_len; i++){
        // In packet proccessing each 16 charcters is a new line.
        if((i % 16) == 0){
            // Don't print ASCII buffer for the "zeroth" line.
            if(i != 0)
                printf("  %s\n", buff);
            // Print offset of line. (e.g. 0000, 0010.)
            printf(" %04x ", i);
        }

        // Print the hex code of the current character.
        printf(" %02x", pc[i]);
        // Add the buffer of the character at the end of the row.
        if((pc[i] < 0x20) || (pc[i] > 0x7e))
            buff[i % 16] = '.';
        else
            buff[i % 16] = pc[i];
        buff[(i % 16) + 1] = '\0';
    }
    // If last line is not 16 character, pad with ' ' characters.
    while((i % 16) != 0){
        printf("   ");
        i++;
    }
    // Print the dump of the packet.
    printf("  %s\n", buff);
}

/**
 * @brief This function is used to construct and IPv4 header for packets.
 * 
 * @param datagram The packet's data stream.
 * @param src_ip Source IP address.
 * @param dst_ip Destination IP address.
 * @param protocol Protocol of the next packet. (e.g. UDP,TCP.)
 * @param pkt_id Id number of the packet.
 * @return struct iphdr* A pointer to the IPv4 header.
 */
struct iphdr *construct_ipv4_header(char *datagram, char src_ip[32], char dst_ip[32], uint8_t protocol,
int pkt_id){

	struct iphdr *iph = (struct iphdr*) datagram;
	iph->ihl = 5;
	iph->version = 4;
	iph->id = htonl(pkt_id);
	iph->ttl = 255;
	iph->protocol = protocol;
	iph->saddr = inet_addr(src_ip); // Soruce IP
	iph->daddr = inet_addr(dst_ip); // Destination IP

	return iph;
}

/**
 * @brief This function is used to construct a UDP packet.
 * 
 * @param dst_ip Destination IP address.
 * @param dst_port Destination port.
 * @param src_ip Source IP address.
 * @param src_port Source port.
 * @param payload Data we want to send with the packet.
 * @param pkt_id Id number of the packet.
 * @return char* Data stream representing the packet.
 */
char *construct_udp_packet(char dst_ip[32], int dst_port, char src_ip[32],
int src_port, char *payload, int pkt_id){

	// Define variables
	char *datagram, *data, *pseudogram;
	struct iphdr *iph;
	struct pseudo_header psh;
	int psize;

	// Allocate memory for the packet
	datagram  = (char *) calloc(MAX_PKTLEN, 1);

	// Copy payload into the datagram
	data = datagram + sizeof(struct iphdr) + sizeof(struct udphdr);
	strcpy(data, payload);

	// Construct IPv4 header
	iph = construct_ipv4_header(datagram, src_ip, dst_ip, IPPROTO_UDP, pkt_id);
	// Fill in total length of packet
	iph->tot_len = sizeof(struct iphdr) + sizeof(struct udphdr) + strlen(payload);

	// Construct UDP header
	struct udphdr *udph = (struct udphdr *) (datagram + sizeof(struct iphdr));
	udph->source = htons(src_port);
	udph->dest = htons(dst_port);
	udph->len = htons(UDP_HDRLEN + strlen(data));
	udph->check = 0;

	// Calculate checksum of IPv4 header
	iph->check = csum((unsigned short*) datagram, iph->tot_len);

	//Now the UDP checksum using the pseudo header
	psh.source_address = inet_addr(src_ip);
	psh.dest_address = inter.sin_addr.s_addr;
	psh.placeholder = 0;
	psh.protocol = IPPROTO_UDP;
	psh.pkt_length = htons(sizeof(struct udphdr) + strlen(data));

	psize = sizeof(struct pseudo_header) + sizeof(struct udphdr) + strlen(data);
	pseudogram = malloc(psize);
	memcpy(pseudogram , (char*) &psh , sizeof (struct pseudo_header));
	memcpy(pseudogram + sizeof(struct pseudo_header) , udph , sizeof(struct udphdr) + strlen(data));

	udph->check = csum( (unsigned short*) pseudogram , psize);
	pkt_len = iph->tot_len;
	return datagram;
}

/**
 * @brief This function recieves a buffer representing a packet and parses it.
 *
 * @param buffer Char array representing the packet.
 * @param src_ip IP address to accept packets from.
 * @param dst_port Port number to accept packets from.
 */
void parse_packet(char *buffer, char *src_ip, int dst_port){

	// Define variables
	unsigned short iphdrlen = IP4_HDR_LEN;
	char *dst_ip, *payload;
	int pkt_len = 0, captured_dst_port;
	struct sockaddr_in source, dest;
	struct ethhdr *eth;
	struct iphdr *iph;
	struct udphdr *udph;
	struct tcphdr *tcph;
	int payload_len = 0;

	eth = (struct ethhdr*) buffer;
	iph = (struct iphdr*) (buffer + ETH_HDR_LEN);
	memset(&source, 0, sizeof(source));
	memset(&dest, 0, sizeof(dest));

	// If the packet is from IP address = src_ip and from port = dst_port
	if (inet_addr(src_ip) == iph->saddr){
		switch(iph->protocol){
			case IPPROTO_TCP:
				tcph = (struct tcphdr*) (buffer + IP4_HDR_LEN);
				captured_dst_port = ntohs(tcph->dest);
				break;
			case IPPROTO_UDP:
				udph = (struct udphdr*) (buffer + IP4_HDR_LEN);
				captured_dst_port = ntohs(udph->dest);
				break;
			default:
				if(verbose_flag)
					perror("Packet protocol is unknown, ignoring.");
				break;
		}
		if (captured_dst_port == dst_port){
			pkt_len = ntohs(iph->tot_len) + ETH_HDR_LEN;
			if (verbose_flag){
				print_ethhdr(eth);
				print_ipv4h(iph);
				switch(iph->protocol){
					case IPPROTO_UDP:
						payload_len = ntohs(iph->tot_len) - (udph->len * 4) - (iph->ihl * 4);
						payload = (char*) malloc(payload_len + 1);
						strncpy(payload, buffer + UDP_HDR_OFFSET, payload_len);
						print_udph(udph, payload);
						break;
					case IPPROTO_TCP:
						payload_len = ntohs(iph->tot_len) - (tcph->doff * 4) - (iph->ihl * 4);
						payload = (char*) malloc(payload_len + 1);
						strncpy(payload, buffer + TCP_HDR_OFFSET, payload_len);
						print_tcph(tcph, payload);
						break;
				}
				packet_dump("RX packet dump", buffer, pkt_len);
			}
		}
	}
}

/**
 * @brief This function will listen to packets for time amount seconds or forever.
 *
 * @param interface_name Name of interface use wants to listen on.
 * @param src_ip IP address of the source packet.
 * @param dst_port The port it the packet is intedent for.
 * @param time Amount of time to listen to, Zero for unlimited until terminated.
 */
void listen_to_traffic(char *interface_name, char *src_ip, int dst_port, int time_to_listen){
    struct sockaddr_in sockstr, their_addr;
    socklen_t socklen, their_addr_len = (socklen_t) sizeof(their_addr);
    int retval = 0;
    char msg[MAX_PKTLEN], *payload;
    ssize_t msglen;
	time_t start = time(NULL);

	open_rx_socket(interface_name);
    memset(msg, 0, MAX_PKTLEN);

	if (time_to_listen != 0){
		time_t seconds = time_to_listen;
		time_t end = start + seconds;
		while(start < end){
			if ((msglen = recvfrom(rx_socketfd, msg, MAX_PKTLEN, 0, (struct sockaddr*) &their_addr, &their_addr_len)) == -1) {
				perror("recv");
				retval = 1;
			}
			parse_packet(msg, src_ip, dst_port);
			start = time(NULL);
		}
	}
	else{
		while(1){
			if ((msglen = recvfrom(rx_socketfd, msg, MAX_PKTLEN, 0, (struct sockaddr*) &their_addr, &their_addr_len)) == -1) {
				perror("recv");
				retval = 1;
			}
			parse_packet(msg, src_ip, dst_port);
		}
	}
}

/**
 * @brief This function prints data of an IPv4 header.
 *
 * @param iph Data structure representing an IPv4 header.
 */
void print_ipv4h(struct iphdr *iph){
		struct sockaddr_in source,dest;
		source.sin_addr.s_addr = iph->saddr;
		dest.sin_addr.s_addr = iph->daddr;
		printf("\nIPv4 Header\n");
		printf("\t|-Packet length: %d\n", ntohs(iph->tot_len));
		printf("\t|-Protocol: %d\n", iph->protocol);
		printf("\t|-Source IP: %s\n", inet_ntoa(source.sin_addr));
		printf("\t|-Destination IP: %s\n", inet_ntoa(dest.sin_addr));
}

/**
 * @brief This function prints data of a UDP header.
 *
 * @param udph Data structure representing a UDP header.
 * @param payload Char array representing the payload of the packet.
 */
void print_udph(struct udphdr *udph, char *payload){

	printf("\nUDP Header\n");
	printf("\t|-Source Port      : %d\n" , ntohs(udph->source));
	printf("\t|-Destination Port : %d\n" , ntohs(udph->dest));
	printf("\t|-UDP Length       : %d\n" , ntohs(udph->len));
	printf("\t|-UDP Checksum     : %d\n" , ntohs(udph->check));
	printf("\t|-Payload          : %s\n\n", payload);
}

/**
 * @brief This function will open a socket for listening on.
 *
 * @param interface_name Name of interface use wants to listen on.
 */
void open_rx_socket(char *interface_name){
	struct sockaddr_in sockstr;
    socklen_t socklen;
	int ret, interface_len = strlen(interface_name)+ 1;

    if ((rx_socketfd = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) == -1) {
        perror("socket");
		exit(rx_socketfd);
    }
	
	ret = setsockopt(rx_socketfd , SOL_SOCKET , SO_BINDTODEVICE , interface_name , interface_len);
	if (ret != 0){
		perror("setsockopt-bind");
		exit(EXIT_FAILURE);
	}
}

/**
 * @brief This function prints data of a TCP header.
 *
 * @param tcph Data structure representing a TCP header.
 * @param payload Char array representing the payload of the packet.
 */
void print_tcph(struct tcphdr *tcph, char *payload){
	printf("\n");
	printf("TCP Header\n");
	printf("	|-Source Port		: %u\n",ntohs(tcph->source));
	printf("	|-Destination Port	: %u\n",ntohs(tcph->dest));
	printf("	|-Sequence Number	: %u\n",ntohl(tcph->seq));
	printf("	|-Acknowledge Number	: %u\n",ntohl(tcph->ack_seq));
	printf("	|-Header Length		: %d BYTES\n" ,(unsigned int)tcph->doff*4);
	printf("	|-Urgent Flag		: %d\n",(unsigned int)tcph->urg);
	printf("	|-Acknowledgement Flag	: %d\n",(unsigned int)tcph->ack);
	printf("	|-Push Flag		: %d\n",(unsigned int)tcph->psh);
	printf("	|-Reset Flag		: %d\n",(unsigned int)tcph->rst);
	printf("	|-Synchronise Flag	: %d\n",(unsigned int)tcph->syn);
	printf("	|-Finish Flag		: %d\n",(unsigned int)tcph->fin);
	printf("	|-Window		: %d\n",ntohs(tcph->window));
	printf("	|-Checksum		: %d\n",ntohs(tcph->check));
	printf("	|-Urgent Pointer	: %d\n",tcph->urg_ptr);
	printf("	|-Payload		: %s\n\n", payload);
}

void print_ethhdr(struct ethhdr *eth){
	printf("\n");
	printf("Ethernet Header\n");
	printf("	|-Destination Address : %.2X-%.2X-%.2X-%.2X-%.2X-%.2X \n", eth->h_dest[0] , eth->h_dest[1] , eth->h_dest[2] , eth->h_dest[3] , eth->h_dest[4] , eth->h_dest[5] );
	printf("	|-Source Address      : %.2X-%.2X-%.2X-%.2X-%.2X-%.2X \n", eth->h_source[0] , eth->h_source[1] , eth->h_source[2] , eth->h_source[3] , eth->h_source[4] , eth->h_source[5] );
	printf("	|-Protocol            : %u \n",(unsigned short)eth->h_proto);
}
