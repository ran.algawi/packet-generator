#include "generator.h"

int main(int argc, char *argv[]){

	char *interface_name = NULL, *dest_mac, *dest_ip, *traffic_kind = NULL, *sec_dest_ip;
	char *src_ip, *payload, *datagram;
	int packet_cnt=-1, flood=-1, d_port=-1, verbose=0, safe=-1;
	int show=0, src_port, listen=-1, i, j;
	float round_interval=-1, round_interval_micro, round_cnt=-1;
	float traffic_time=-1;

	if(argc == 1){
		printf("No arguments passed to app, exiting.\n");
		exit(EXIT_MISSARG);
	}

	if(strcmp(argv[1],"-h") == 0 || strcmp(argv[1], "--help") == 0){
		print_help();
		exit(EXIT_SUCCESS);
	}


	if(strcmp(argv[1], "--ecodes") == 0){
		print_exit_codes();
		exit(EXIT_SUCCESS);
	}

	// Assign variables from command line
	for(int i=1; i<argc;i+=2){
		if(strcmp(argv[i], "-pc") == 0)
			packet_cnt = atoi(argv[i+1]);
		else  if(strcmp(argv[i], "-r") == 0)
			round_cnt = atof(argv[i+1]);
		else if(strcmp(argv[i], "-i") == 0)
			round_interval = atof(argv[i+1]);
		else if(strcmp(argv[i], "-nic") == 0)
			interface_name = argv[i+1];
		else if(strcmp(argv[i], "-ip") == 0)
			dest_ip = argv[i+1];
		else if(strcmp(argv[i], "-mac") == 0)
			dest_mac = argv[i+1];
		else if(strcmp(argv[i], "-f") == 0){
			flood = 1;
			i--;
		}
		else if(strcmp(argv[i], "-t") == 0)
			traffic_kind = argv[i+1];
		else if(strcmp(argv[i], "-dp") == 0)
			d_port = atoi(argv[i+1]);
		else if(strcmp(argv[i], "--time") == 0)
			traffic_time = atof(argv[i+1]);
		else if(strcmp(argv[i], "-v") == 0){
			verbose_flag = 1;
			i--;
		}
		else if(strcmp(argv[i], "--safe") == 0){
			safe=1;
			i--;
		}
		else if(strcmp(argv[i], "-s") == 0){
			show = 1;
			i--;
		
		}
		else if(strcmp(argv[i], "-ipin") == 0)
			sec_dest_ip = argv[i+1];
		else if(strcmp(argv[i], "-sp") == 0)
			src_port = atoi(argv[i+1]);
		else if(strcmp(argv[i], "--payload") == 0)
			payload = argv[i+1];
		else if(strcmp(argv[i], "--listen") == 0){
			listen = 1;
			i--;
		}
		else{
			printf("No such flag '%s', Please check input\n", argv[i]);
			exit(EXIT_FAILURE);
		}
	}

	if(interface_name == NULL){
		printf("No name was give for soruce interface, using flag -nic\n");
		exit(EXIT_FAILURE);
	}
	if(traffic_kind == NULL){
		printf("No traffic kind was given, Please see usage with flag -h\n");
		exit(EXIT_FAILURE);
	}

	if(safe == 1){
		if(packet_cnt == -1){
			packet_cnt = 100;
			printf("No value was given for packet count, using default: 100\n");
		}
		if(round_cnt == -1){
			round_cnt = 10;
			printf("No value was given for round count, using default: 10\n");
		}
		if(round_interval == -1){
			round_interval = 3;
			printf("No value was given for round interval, using default: 3 seconds\n");
		}
	}

	// If flood flag was used, override packet count and round interval.
	if(flood==1){
		if (traffic_time == -1){
			printf("Flood flag is on but no time was given using flag --time\n");
			exit(EXIT_FAILURE);
		}
		else if(traffic_time == -1 && safe == 1)
			traffic_time = 60;
		round_interval = 0;
		packet_cnt = 10000;
	}

	src_ip = get_ip_of_interface(interface_name);
	setup_v4_interface(d_port, dest_ip, interface_name);

	if (payload == NULL){
		payload=(char*) malloc(1);
		payload[0]='\0';
	}

	if(listen == -1){
		open_tx_socket();
		if(strcpy(traffic_kind, "udp") == 0)
			datagram = construct_udp_packet(dest_ip, d_port, src_ip, src_port, payload, 0);
		else
			datagram = construct_tcp_packet(dest_ip, d_port, src_ip, src_port, payload, 0, 0, 0, 0, 0, 0, 0);
		for(i=0; i < round_cnt; i++){
			for(j=0; j < packet_cnt; j++)
				send_packet(datagram, pkt_len);
			sleep(round_interval);
		}
	}
	else
	{
		if(traffic_time == -1 && safe != 1){
			printf("No amount of time was given for listening\n");
			exit(EXIT_MISSARG);
		}
		else if(traffic_time == -1)
			traffic_time = 60;
		listen_to_traffic(interface_name, dest_ip, d_port, traffic_time);
	}
}
