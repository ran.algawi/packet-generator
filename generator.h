#ifndef generator
#define generator

// Include header files
#include<stdio.h>
#include<string.h>                   	   // memset
#include<stdlib.h> 						 // exit function
#include<net/if.h>
#include<sys/ioctl.h>
#include<netinet/ip.h>	          	 // ip header
#include<netinet/tcp.h>
#include<netinet/udp.h>
#include<arpa/inet.h>              // inet_ntoa, inet_addr
#include<unistd.h>               // sleep function
#include<linux/if_ether.h>      // ethernet frame header
#include<linux/if_packet.h>    // saddr_ll header
#include<linux/types.h>
#include<ctype.h>
#include<time.h>
#include<ifaddrs.h>
#include <sys/signal.h>	   // Use signals


// Define constant variables and globals
#define EXIT_MISSARG 40
#define MAX_PKTLEN 4096
#define UDP_HDRLEN 8
#define SA struct sockaddr
#define TCP_OFFSET 5
#define PKT_SEND_FAIL 5
#define PKT_NEG_LEN 6
#define PKT_ZERO_LEN 7
#define ETH_HDR_LEN sizeof(struct ethhdr)
#define IP4_HDR_LEN sizeof(struct iphdr) + ETH_HDR_LEN
#define TCP_HDR_OFFSET IP4_HDR_LEN + sizeof(struct tcphdr)
#define UDP_HDR_OFFSET IP4_HDR_LEN + sizeof(struct udphdr)

// Socket variable should be global so it can be closed from anywhere.
int tx_socketfd;
int rx_socketfd;
int pkt_len;

// Verbose variable should be global so any function can read it.
int verbose_flag;

// interface struct
struct ifreq ifr;
struct sockaddr_in inter;

// Define structs
// pseudo header which will be used to calculate checksum.
struct pseudo_header
{
	u_int32_t source_address;
	u_int32_t dest_address;
	u_int8_t placeholder;
	u_int8_t protocol;
	u_int16_t pkt_length;
};


// Define functions
void print_welcome();
void print_help();
void print_exit_codes();
void print_variables_used_for_generator(int pc, int r, float i, char* nic, 
		char* ip, char* mac, char* traffic_kind, int traffic_time);
unsigned char hex_digit(char ch);
unsigned short csum(unsigned short *ptr,int nbytes);
char* get_ip_of_interface(char* interface_name);
void lower_string(char* str);
void close_down(int sigtype);
void open_tx_socket();
char *construct_tcp_packet(char dst_ip[32], int dst_port, char src_ip[32], 
int src_port, char *payload, int sequence_num, int syn_flag, int ack_flag,
int ack_num, int fin_flag, int rst_flag, int pkt_id);
void setup_v4_interface(int port, char *ip, char* interface_name);
void send_packet(char *datagram, int iph_len);
void packet_dump (const char *desc, const void *pkt, const int pkt_len);
struct iphdr *construct_ipv4_header(char *datagram, char src_ip[32], char dst_ip[32], uint8_t protocol,
int pkt_id);
char *construct_udp_packet(char dst_ip[32], int dst_port, char src_ip[32],
int src_port, char *payload, int pkt_id);
void parse_packet(char *buffer, char *src_ip, int dst_port);
void listen_to_traffic(char *interface_name, char *src_ip, int dst_port, int time_to_listen);
void print_ipv4h(struct iphdr *iph);
void print_udph(struct udphdr *udph, char *payload);
void open_rx_socket(char *interface_name);
void print_tcph(struct tcphdr *tcph, char *payload);
void print_ethhdr(struct ethhdr *eth);
#endif
