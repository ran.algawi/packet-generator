include config.mak
objects = main_gen.c generator.h generator.c

x86_64:
	gcc -g $(objects) -o packet_generator_x86_64 -std=gnu99
arm64:
	$(cross-compiler) $(objects) -o packet_generator_arm64
all:   | arm64 x86_64
clean:
	rm *packet_generator*
fresh: | clean arm64 x86_64
